package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh01N017.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh01N017Test {
    public static void main(String args[]) {
        testO();
        testP();
        testR();
        testS();
    }

    private static void testO() {
        assertEquals("TaskCh01N017PointO", 1, getResultO(0));
    }

    private static void testP() {
        assertEquals("TaskCh01N017PointP", 0.5, getResultP(2, 1, 0, 0));
    }

    private static void testR() {
        assertEquals("TaskCh01N017PointR", 0.5403023058681398, getResultR(1));
    }

    private static void testS() {
        assertEquals("TaskCh01N017PointS", 3, getResultS(1));
    }
}