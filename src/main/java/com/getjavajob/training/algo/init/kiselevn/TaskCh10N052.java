package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(revNat(sc.nextInt()));
    }

    static int revNat(int n) {
        if (n / 10 != 0) {
            int temp = pow(10, TaskCh10N043.numberOfDigits(n) - 1);
            n = revNat(n / 10) + n % 10 * temp;
        }
        return n;
    }

    private static int pow(int a, int n) {
        return n == 1 ? a : a * pow(a, n - 1);
    }
}