package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println(printNum(input.nextInt()));
    }

    public static int printNum(int i) {
        System.out.println("The number you entered is " + i);
        return i;
    }
}