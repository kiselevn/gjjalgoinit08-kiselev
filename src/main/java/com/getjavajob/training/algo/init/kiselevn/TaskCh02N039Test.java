package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh02N039.getAngle;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh02N039Test", 90, getAngle(3, 0.0, 0.0));
    }
}