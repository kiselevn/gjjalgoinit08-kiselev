package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String args[]) {
        Scanner sc = new java.util.Scanner(System.in);
        System.out.println(whichSeason(sc.nextInt()));
    }

    static String whichSeason(int input) {
        switch (input % 12 / 3) {
            case 0:
                return "Winter";
            case 1:
                return "Spring";
            case 2:
                return "Summer";
            case 3:
                return "Autumn";
        }
        return "Nan";
    }
}