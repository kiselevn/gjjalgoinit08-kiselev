package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh09N166 {
    public static void main(String args[]) {
        System.out.println(replaceWords("LOL mLG"));
    }

    static String replaceWords(String input) {
        String[] words = input.split(" ");
        StringBuilder result = new StringBuilder(input.length()).append(words[words.length - 1]).append(" ");
        for (int i = 1; i < words.length - 1; i++) {
            result.append(words[i]).append(" ");
        }
        result.append(words[0]);
        return result.toString();
    }
}