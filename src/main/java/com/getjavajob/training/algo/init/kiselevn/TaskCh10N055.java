package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh10N055 {
    public static void main(String args[]) {
        int input = 0x11;
        input = notationConversion(input, 10);
        System.out.println(Integer.toHexString(input));
    }

    static int notationConversion(int input, int numberOfNotation) {
        if (input % numberOfNotation != 0 || input / numberOfNotation != 0) {
            input = input % numberOfNotation + 16 * notationConversion((input / numberOfNotation), numberOfNotation);
        }
        return input;
    }
}