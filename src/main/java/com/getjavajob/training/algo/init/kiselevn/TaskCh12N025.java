package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N025 {
    public static void main(String args[]) {
        int arrLength = 5;
        int[][] input = getResult6(arrLength);
        for (int i = 0; i < arrLength; i++) {
            for (int j = 0; j < arrLength; j++) {
                System.out.print(input[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] getResult1(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[i][j] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult2(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[j][i] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult3(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[i][n - 1 - j] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult4(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[n - j - 1][i] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult5(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 0) {
                    result[i][j] = counter;
                    counter++;
                } else {
                    result[i][n - j - 1] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult6(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 0) {
                    result[j][i] = counter;
                    counter++;
                } else {
                    result[n - j - 1][i] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult7(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[n - i - 1][j] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult8(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[j][n - i - 1] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult9(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[n - i - 1][n - j - 1] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult10(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[n - j - 1][n - i - 1] = counter;
                counter++;
            }
        }
        return result;
    }

    private static int[][] getResult11(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 0) {
                    result[n - i - 1][j] = counter;
                    counter++;
                } else {
                    result[n - i - 1][n - 1 - j] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult12(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 != 0) {
                    result[i][j] = counter;
                    counter++;
                } else {
                    result[i][n - 1 - j] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult13(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 0) {
                    result[j][n - 1 - i] = counter;
                    counter++;
                } else {
                    result[n - j - 1][n - 1 - i] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult14(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 == 1) {
                    result[j][i] = counter;
                    counter++;
                } else {
                    result[n - j - 1][i] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult15(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 != 0) {
                    result[n - i - 1][j] = counter;
                    counter++;
                } else {
                    result[n - i - 1][n - j - 1] = counter;
                    counter++;
                }
            }
        }
        return result;
    }

    private static int[][] getResult16(int n) {
        int[][] result = new int[n][n];
        int counter = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i % 2 != 0) {
                    result[j][n - i - 1] = counter;
                    counter++;
                } else {
                    result[n - j - 1][n - i - 1] = counter;
                    counter++;
                }
            }
        }
        return result;
    }
}