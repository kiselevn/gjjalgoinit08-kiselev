package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N046 {

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(elemGeom(sc.nextInt(), sc.nextInt(), sc.nextInt()));
        System.out.println(sumGeom(sc.nextInt(), sc.nextInt(), sc.nextInt()));
    }

    static double elemGeom(double firstElem, double difference, int n) {
        return n == 1 ? firstElem : difference * elemGeom(firstElem, difference, n - 1);
    }

    static double sumGeom(double sum, double difference, int n) {
        return n == 0 ? 0 : Math.pow(difference, n - 1) * sum + sumGeom(sum, difference, n - 1);
    }
}