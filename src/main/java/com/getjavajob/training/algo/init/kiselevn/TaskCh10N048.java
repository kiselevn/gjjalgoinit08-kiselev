package com.getjavajob.training.algo.init.kiselevn;

import java.util.Arrays;

public class TaskCh10N048 {
    public static void main(String args[]) {
        int[] input = {1, 2, 3, 70, 90, 6, -76, 96, 145, 9000};
        System.out.println(maxOfArray(input));
    }

    static int maxOfArray(int[] input) {
        switch (input.length) {
            case 1:
                return input[0];
            case 2:
                return Math.max(input[0], input[1]);
            default:
                return Math.max(maxOfArray(Arrays.copyOf(input, input.length / 2)), maxOfArray(Arrays.copyOfRange(input, input.length / 2, input.length)));
        }
    }
}