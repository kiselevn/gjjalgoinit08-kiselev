package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        Game test = new Game("Kanabians", "MLGS");
        assertEquals("TaskCh06N087 result output test", "Game did not reveal the winner. Score 0-0", test.result());
        assertEquals("TaskCh06N087 score output test", "0-0", test.score());
    }
}