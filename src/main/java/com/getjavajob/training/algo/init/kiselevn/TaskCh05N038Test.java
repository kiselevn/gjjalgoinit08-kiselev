package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh05N038.strangeHusband;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N038Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        double[] expected = {1.5, 0.5};
        assertEquals("TaskCh05N038Test", expected, strangeHusband(2));
    }
}