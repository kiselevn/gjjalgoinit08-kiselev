package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh09N107 {
    public static void main(String args[]) {
        System.out.println(replaceLastOFirstA("ooooooooooooooa"));
    }

    static String replaceLastOFirstA(String input) {
        int pos = input.indexOf('a');
        StringBuilder result = new StringBuilder(input).deleteCharAt(pos).insert(pos, 'o');
        pos = input.lastIndexOf('o');
        result.deleteCharAt(pos).insert(pos, 'a');
        return result.toString();
    }
}