package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh09N022.firstHalf;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N022Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N017Test", "Bomb", firstHalf("Bombanan"));
    }
}