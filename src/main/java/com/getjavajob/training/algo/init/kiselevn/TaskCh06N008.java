package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getResult(sc.nextInt()));
    }

    /** @result gives a row of squares < N */
    static String getResult(int input) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= Math.sqrt(input); i++) {
            result.append(i * i).append(" ");
        }
        return result.toString();
    }
}