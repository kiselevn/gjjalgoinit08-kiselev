package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N045.elemArith;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N045.sumArith;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N045Test elemArith", 60, elemArith(1, 1, 60));
        assertEquals("TaskCh10N045Test sumArith", 6, sumArith(2, 2, 2));
    }
}