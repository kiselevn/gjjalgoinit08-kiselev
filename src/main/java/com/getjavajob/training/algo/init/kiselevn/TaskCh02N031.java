package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println(getResult(input.nextInt()));
    }

    static int getResult(int input) {
        return input / 100 * 100 + input % 10 * 10 + input / 10 % 10;
    }
}