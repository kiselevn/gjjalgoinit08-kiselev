package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N043.digitSum;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N043.numberOfDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N043Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N043Test digitSum", 6, digitSum(312));
        assertEquals("TaskCh10N043Test numberOfDigits ", 3, numberOfDigits(322));
    }
}