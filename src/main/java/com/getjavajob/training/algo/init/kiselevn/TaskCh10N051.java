package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N051 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        getResultC(sc.nextInt());
    }

    private static void getResultA(int input) {
        if (input > 0) {
            System.out.println(input);
            getResultA(input - 1);
        }
    }

    private static void getResultB(int input) {
        if (input > 0) {
            getResultB(input - 1);
            System.out.println(input);
        }
    }

    private static void getResultC(int input) {
        if (input > 0) {
            System.out.println(input);
            getResultC(input - 1);
        }
        System.out.println(input);
    }
}