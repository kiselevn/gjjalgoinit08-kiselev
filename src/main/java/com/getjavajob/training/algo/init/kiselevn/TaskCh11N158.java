package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh11N158 {
    public static void main(String args[]) {
        String[] input = {"21", "17", "20", "hu", "17", "20", "17", "hu"};
        input = deleteRepeatingElements(input);
        for (String anInput : input) {
            System.out.println(anInput);
        }
    }

    static String[] deleteRepeatingElements(String[] input) {
        for (int i = input.length - 1; i >= 0; i--) {
            if (numberOfOccurrences(input, input[i]) > 1) {
                input = removal(input, i);
            }
        }
        return input;
    }

    private static int numberOfOccurrences(String[] input, String instance) {
        int n = 0;
        for (String anInput : input) {
            if (anInput.equals(instance)) {
                n++;
            }
        }
        return n;
    }

    private static String[] removal(String[] input, int number) {
        input[number] = "0";
        for (int i = number; i < input.length - 1; i++) {
            String temp = input[i];
            input[i] = input[i + 1];
            input[i + 1] = temp;
        }
        return input;
    }
}