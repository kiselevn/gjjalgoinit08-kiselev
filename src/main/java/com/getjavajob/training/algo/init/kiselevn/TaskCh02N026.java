package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh02N026 {
    public static void main(String args[]) {
        System.out.println(getResult(564));
    }

    static int getResult(int x) {
        return x % 10 * 100 + x / 10;
    }
}