package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh11N245 {
    public static void main(String[] args) {
    }

    static double[] negativesFirst(double[] input) {
        double[] result = new double[input.length];
        int negativeStackNum = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i] < 0) {
                result[negativeStackNum] = input[i];
                negativeStackNum++;
            } else {
                result[input.length - i + negativeStackNum - 1] = input[i];
            }
        }
        return result;
    }
}