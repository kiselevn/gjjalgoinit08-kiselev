package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N055.notationConversion;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N055Test", 0x1000, notationConversion(8, 2));
    }
}