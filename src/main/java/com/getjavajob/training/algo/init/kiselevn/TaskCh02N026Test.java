package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh02N026.getResult;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh02N026Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh02N026", 456, getResult(564));
    }
}