package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N044.digitSqrt;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N044Test", 6, digitSqrt(78));
    }
}