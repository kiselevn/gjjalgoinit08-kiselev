package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh02N013.numReverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh02N013Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh02N013Test", 657, numReverse(756));
    }
}