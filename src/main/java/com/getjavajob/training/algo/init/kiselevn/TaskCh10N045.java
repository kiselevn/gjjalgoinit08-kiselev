package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(elemArith(sc.nextInt(), sc.nextInt(), sc.nextInt()));
        System.out.println(sumArith(sc.nextInt(), sc.nextInt(), sc.nextInt()));
    }

    static double elemArith(double firstElem, double difference, int n) {
        return n == 1 ? firstElem : difference + elemArith(firstElem, difference, n - 1);
    }

    static double sumArith(double sum, double difference, int n) {
        return n == 0 ? 0 : difference * (n - 1) + sum + sumArith(sum, difference, n - 1);
    }
}