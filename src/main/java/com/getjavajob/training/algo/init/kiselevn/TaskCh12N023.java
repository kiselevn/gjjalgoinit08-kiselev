package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N023 {
    public static void main(String args[]) {
        int n = 45;
        int[][] input = getResultC(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(input[i][j] + " ");
            }
            System.out.println();
        }
    }

    static int[][] getResultA(int n) {
        int[][] result = new int[n][n];
        for (int i = 0; i < n; i++) {
            result[i][i] = 1;
            result[i][n - i - 1] = 1;
        }
        return result;
    }

    static int[][] getResultB(int n) {
        int[][] result = new int[n][n];
        for (int i = 0; i < n; i++) {
            result[i][n / 2] = 1;
            result[n / 2][i] = 1;
            result[i][i] = 1;
            result[i][n - i - 1] = 1;
        }
        return result;
    }

    static int[][] getResultC(int n) {
        int[][] result = new int[n][n];
        for (int i = 0; i < n / 2 + 1; i++) {
            for (int j = 0; j < n; j++) {
                if (Math.abs(j - n / 2) < n / 2 - i + 1) {
                    result[i][j] = 1;
                    result[n - i - 1][j] = 1;
                }
            }
        }
        return result;
    }
}