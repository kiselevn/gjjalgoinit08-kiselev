package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N106.whichSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N067Test", "Summer", whichSeason(7));
    }
}