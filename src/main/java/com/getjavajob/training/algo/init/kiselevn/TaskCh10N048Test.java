package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N048.maxOfArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[] temp = {1, 2, 3, 4, 5, 6, 60};
        assertEquals("TaskCh10N048Test", 60, maxOfArray(temp));
    }
}