package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh09N185 {
    public static void main(String args[]) {
        System.out.println(bracesDoneRight("((87-17-1)))"));
    }

    static int wrongBraceNum(String input) {
        int bracesNum = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '(') {
                bracesNum++;
            } else if (input.charAt(i) == ')') {
                bracesNum--;
            }
            if (bracesNum < 0) {
                return i + 1;
            }
        }
        if (bracesNum > 0) {
            return bracesNum;
        }
        return 0;
    }

    static boolean bracesDoneRight(String input) {
        boolean result = true;
        int bracesNum = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '(') {
                bracesNum++;
            } else if (input.charAt(i) == ')') {
                bracesNum--;
            }
        }
        return bracesNum != 0 ? result : true;
    }
}