package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(digitSqrt(sc.nextInt()));
    }

    public static int digitSqrt(int n) {
        return n / 10 == 0 ? n : digitSqrt(TaskCh10N043.digitSum(n));
    }
}