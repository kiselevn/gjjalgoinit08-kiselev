package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter team #1:");
        String team1 = sc.nextLine();
        System.out.println("Enter team #2:");
        String team2 = sc.nextLine();
        System.out.println(getResult(team1, team2));
    }

    /** @return method that leads the game*/
    private static String getResult(String team1, String team2) {
        Game game = new Game(team1, team2);
        while (game.currentTeam != 0) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            Scanner scanner = new java.util.Scanner(System.in);
            game.currentTeam = scanner.nextInt();
            game.play();
            System.out.println(game.score());
        }
        return game.result();
    }
}

class Game {
    int currentTeam;
    private int score1;
    private int score2;
    private String team1;
    private String team2;

    public Game(String team1, String team2) {
        currentTeam = 1;
        this.team1 = team1;
        this.team2 = team2;
    }

    void play() {
        Scanner sc = new java.util.Scanner(System.in);
        System.out.println("Enter score (1 or 2 or 3):");
        switch (currentTeam) {
            case 1:
                score1 += sc.nextInt();
                break;
            case 2:
                score2 += sc.nextInt();
                break;
            default:
                break;
        }
    }

    String score() {
        return score1 + "-" + score2;
    }

    String result() {
        String res = "";
        if (score1 > score2) {
            res = "Team " + team1 + " won " + team2 + " with the score " + score1 + "-" + score2;
        } else if (score1 == score2) {
            res = "Game did not reveal the winner. Score " + score1 + "-" + score2;
        } else {
            res = "Team " + team2 + " won " + team1 + " with the score " + score1 + "-" + score2;
        }
        return res;
    }
}