package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh05N010.transferTable;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N010Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        double[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        assertEquals("TaskCh05N010Test", expected, transferTable(1,20));
    }
}