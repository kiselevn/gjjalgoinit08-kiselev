package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(trafficLight(sc.nextInt()));
    }

    static String trafficLight(int input) {
        return input % 5 < 3 ? "Green" : "Red";
    }
}