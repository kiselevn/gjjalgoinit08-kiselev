package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N234 {
    public static void main(String args[]) {
        int[][] input = {{1, 3, 4}, {3, 5, 0}, {7, 8, 9}};
        input = deleteRow(input, 1);
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                System.out.print(input[i][j] + " ");
            }
            System.out.println();
        }
    }

    static int[][] deleteColumn(int[][] input, int n) {
        for (int i = n; i < input[0].length; i++) {
            for (int j = 0; j < input.length; j++) {
                int temp = input[j][i];
                input[j][i] = input[j][i - 1];
                input[j][i - 1] = temp;
            }
        }
        for (int i = 0; i < input.length; i++) {
            input[i][input[0].length - 1] = 0;
        }
        return input;
    }

    static int[][] deleteRow(int[][] input, int n) {
        for (int i = n; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                int temp = input[i][j];
                input[i][j] = input[i - 1][j];
                input[i - 1][j] = temp;
            }
        }
        for (int i = 0; i < input[0].length; i++) {
            input[input.length - 1][i] = 0;
        }
        return input;
    }
}