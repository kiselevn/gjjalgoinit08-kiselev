package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(firstHalf(sc.nextLine()));
    }

    static String firstHalf(String input) {
        return input.substring(0, (input.length() + 1) / 2);
    }
}