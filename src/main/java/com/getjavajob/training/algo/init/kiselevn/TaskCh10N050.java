package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(ackermann(sc.nextInt(), sc.nextInt()));
    }

    static int ackermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return ackermann(m - 1, 1);
        } else {
            return ackermann(m - 1, ackermann(m, n - 1));
        }
    }
}