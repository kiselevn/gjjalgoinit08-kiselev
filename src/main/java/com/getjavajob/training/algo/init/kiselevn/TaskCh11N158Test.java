package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh11N158.deleteRepeatingElements;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N158Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        String[] temp1 = {"21", "17", "20", "hu", "17", "20", "17", "hu"};
        String[] temp2 = {"21", "17", "20", "hu"};
        assertEquals("TaskCh11N158Test", temp2, deleteRepeatingElements(temp1));
    }
}