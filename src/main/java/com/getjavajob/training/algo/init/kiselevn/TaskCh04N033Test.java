package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N033.getResultA;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N033.getResultB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N033Test {
    public static void main(String args[]) {
        testA();
        testB();
    }

    private static void testA() {
        assertEquals("TaskCh04N33TestA true", true, getResultA(4590));
        assertEquals("TaskCh04N33TestA false", false, getResultA(4591));
    }

    private static void testB() {
        assertEquals("TaskCh04N33TestA true", false, getResultB(4590));
        assertEquals("TaskCh04N33TestA false", true, getResultB(4591));
    }
}