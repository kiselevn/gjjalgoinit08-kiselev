package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(fact(sc.nextInt()));
    }

    static int fact(int n) {
        return n == 1 ? 1 : n * fact(n - 1);
    }
}