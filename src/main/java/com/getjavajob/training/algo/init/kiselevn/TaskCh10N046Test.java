package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N046.elemGeom;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N046.sumGeom;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N046Test elemGeom", 4, elemGeom(1, 2, 3));
        assertEquals("TaskCh10N046Test sumGeom", 6, sumGeom(2, 2, 2));
    }
}