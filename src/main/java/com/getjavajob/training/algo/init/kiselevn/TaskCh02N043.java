package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println(divisiblityTest(input.nextInt(), input.nextInt()));
    }

    static int divisiblityTest(int a, int b) {
        return (a%b)*(b%a)+1;
    }
}