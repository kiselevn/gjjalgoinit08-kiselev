package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N234.deleteRow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[][] input = new int[][]{{1, 3, 4}, {3, 5, 7}, {7, 8, 9}};
        int[][] expectedA = new int[][]{{1, 3, 0}, {3, 5, 0}, {7, 8, 0}};
        assertEquals("TaskChN234Test delete column", expectedA, deleteColumn(input, 3));
        input = new int[][]{{1, 3, 4}, {3, 5, 7}, {7, 8, 9}};
        int[][] expectedB = new int[][]{{3, 5, 7}, {7, 8, 9}, {0, 0, 0}};
        assertEquals("TaskChN234Test delete row", expectedB, deleteRow(input, 1));
    }
}