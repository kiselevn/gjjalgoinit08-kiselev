package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N063.getAverage;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        double[] expected = {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};
        int[][] input = new int[][]{
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4},
                {1, 3, 4, 4}
        };
        assertEquals("TaskCh12N063 Test", expected, getAverage(input));
    }
}