package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh10N049 {
    public static void main(String args[]) {
        int[] input = {1, 2, 3, 1, 900};
        System.out.println(indexOfMax(input, 0, input.length - 1));
    }

    static int indexOfMax(int[] input, int initNumber, int endNumber) {
        switch (endNumber - initNumber) {
            case 0:
                return endNumber;
            case 1:
                return input[endNumber] >= input[initNumber] ? endNumber : initNumber;
            default:
                int res1 = indexOfMax(input, initNumber, (endNumber + initNumber) / 2);
                int res2 = indexOfMax(input, (endNumber + initNumber) / 2, endNumber);
                return input[res1] > input[res2] ? res1 : res2;
        }
    }
}