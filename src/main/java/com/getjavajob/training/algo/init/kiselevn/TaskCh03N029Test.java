package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029Test {
    public static void main(String args[]) {
        testA();
        testB();
        testC();
        testD();
        testE();
        testF();
    }

    private static void testA() {
        assertEquals("TaskCh03N029TestA X and Y are odd", true, getResultA(7, 9));
        assertEquals("TaskCh03N029TestA X and Y are even", false, getResultA(8, 10));
        assertEquals("TaskCh03N029TestA X is even", false, getResultA(8, 11));
    }

    private static void testB() {
        assertEquals("TaskCh03N029TestB only X<20", true, getResultB(18, 20));
        assertEquals("TaskCh03N029TestB X and Y >20", false, getResultB(21, 90));
        assertEquals("TaskCh03N029TestB X and Y <20", false, getResultB(8, 10));
    }

    private static void testC() {
        assertEquals("TaskCh03N029TestC X =0", true, getResultC(0, 9));
        assertEquals("TaskCh03N029TestC Y =0", true, getResultC(27, 0));
        assertEquals("TaskCh03N029TestC X and Y !=0", false, getResultC(8, 10));
    }

    private static void testD() {
        assertEquals("TaskCh03N029TestD X,Y,Z>0", false, getResultD(7, 9, 10));
        assertEquals("TaskCh03N029TestD X,Y,Z<0", true, getResultD(-8, -10, -6));
    }

    private static void testE() {
        assertEquals("TaskCh03N029TestD only x%5==0", true, getResultE(10, 3, 4));
        assertEquals("TaskCh03N029TestD only y%5==0", true, getResultE(1, 5, 4));
        assertEquals("TaskCh03N029TestD only z%5==0", true, getResultE(17, 3, 50));
        assertEquals("TaskCh03N029TestD x,y%5==0", false, getResultE(5, 15, -6));
        assertEquals("TaskCh03N029TestD x,z%5==0", false, getResultE(5, 16, 100));
        assertEquals("TaskCh03N029TestD z,y%5==0", false, getResultE(17, 15, 10));
        assertEquals("TaskCh03N029TestD x,y,z%5==0", false, getResultE(5, 15, 90));
        assertEquals("TaskCh03N029TestD x,y,z%5!=0", false, getResultE(6, 11, 9));
    }

    private static void testF() {
        assertEquals("TaskCh03N029TestD X>100", true, getResultF(102, 10, 12));
        assertEquals("TaskCh03N029TestD Y>100", true, getResultF(10, 103, 12));
        assertEquals("TaskCh03N029TestD Z>100", true, getResultF(10, 909, 12));
        assertEquals("TaskCh03N029TestD X,Y>100", true, getResultF(102, 1700, 12));
        assertEquals("TaskCh03N029TestD Z,Y>100", true, getResultF(10, 1700, 122));
        assertEquals("TaskCh03N029TestD Z,X>100", true, getResultF(102, 17, 1267));
        assertEquals("TaskCh03N029TestD X,Y,Z>100", true, getResultF(102, 109, 1800));
        assertEquals("TaskCh03N029TestD X,Y,Z=<100", false, getResultF(-8, 100, -6));
    }
}