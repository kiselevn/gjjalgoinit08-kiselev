package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        short y = input.nextShort();
        byte m = input.nextByte();
        short birthYear = input.nextShort();
        byte birthMonth = input.nextByte();
        System.out.println(getYear(y, m, birthYear, birthMonth));
    }

    public static int getYear(int birthYear, int birthMon, int todYear, int todMon) {
        int result = todYear - birthYear;
        return todMon >= birthMon ? result : result - 1;
    }
}