package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N056.isPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N056Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N056Test", 1, isPrime(17, 15));
    }
}