package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N049.indexOfMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[] temp = {1, 2, 3, 4, 5, 6, 60};
        assertEquals("TaskCh10N049Test", 6, indexOfMax(temp, 0, temp.length - 1));
    }
}