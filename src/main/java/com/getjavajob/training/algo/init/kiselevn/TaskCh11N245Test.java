package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh11N245.negativesFirst;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        double[] temp1 = {10, 12, -6, 13, -4};
        double[] temp2 = {-6, -4, 13, 12, 10};
        assertEquals("TaskCh11N245Test", temp2, negativesFirst(temp1));
    }
}