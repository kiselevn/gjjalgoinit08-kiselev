package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println(getAngle(input.nextByte(), input.nextByte(), input.nextByte()));
    }

    static double getAngle(int h, double m, double s) {
        return h % 12 * 30 + m / 2 + s / 120;
    }
}