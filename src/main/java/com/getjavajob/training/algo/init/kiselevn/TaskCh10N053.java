package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh10N053 {
    public static void main(String args[]) {
        double[] input = {6, 7, 8, 9, 10, 101, 23};
        input = reverseArray(input, 0);
    }

    static double[] reverseArray(double[] input, int n) {
        if (n == input.length - 1) {
            return input;
        }
        for (int i = 0; i < input.length - 1 - n; i++) {
            input[i] = input[i] + input[i + 1];
            input[i + 1] = input[i] - input[i + 1];
            input[i] = input[i] - input[i + 1];
        }
        return reverseArray(input, n + 1);
    }
}