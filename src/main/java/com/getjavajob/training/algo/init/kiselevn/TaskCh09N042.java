package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh09N042 {
    public static void main(String args[]) {
        System.out.println(wordInverser("CORRECT STUFF"));
    }

    static String wordInverser(String input) {
        return new StringBuilder(input).reverse().toString();
    }
}