package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N067.whatDayIsThis;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N067Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N067Test", "Day off", whatDayIsThis(7));
        assertEquals("TaskCh04N067Test", "WorkingDay", whatDayIsThis(5));
    }
}