package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh09N042.wordInverser;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N017Test", "Bomb", wordInverser("bmoB"));
    }
}