package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N028 {
    public static void main(String args[]) {
        int size = 9;
        int[][] input = new int[size][size];
        input = getResult(input, size, size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(input[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static int[][] getResult(int[][] input, int size, int n) {
        if (n == 0) {
            input[size / 2][size / 2] = size * size;
            return input;
        }
        int stepSize = n - (size - n + 1);
        int counter = size * size - (stepSize + 1) * (stepSize + 1) + 1;
        for (int i = 0; i < stepSize; i++) {
            input[size - n][size - n + i] = counter + i;
            input[size - n + i][n - 1] = counter + stepSize + i;
            input[n - 1][n - 1 - i] = counter + stepSize * 2 + i;
            input[n - 1 - i][size - n] = counter + stepSize * 3 + i;
        }
        input = getResult(input, size, n - 1);
        return input;
    }
}