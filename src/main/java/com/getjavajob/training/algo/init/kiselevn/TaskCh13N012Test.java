package com.getjavajob.training.algo.init.kiselevn;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

class TaskCh13N012Test {
    public static void main(String args[]) {
        test();
    }
    private static void test() {
        List<Employee> input = new ArrayList<Employee>();
        input.add(new Employee("Sonniy", "Nicko", "Ivanovich", "Moscow", 3, 10));
        input.add(new Employee("Bodriy", "Murat", "Azatovich", "Army", 5, 3));
        Database secondDeptDatabase = new Database(input);
        List<Employee> expectedOutput = new ArrayList<Employee>();
        expectedOutput.add(input.get(0));
        assertEquals("TaskCh13N012 parseName test", expectedOutput, secondDeptDatabase.searchEmployeeByName("Nick"));
        assertEquals("TaskCh13N012 nYears workers test", expectedOutput, secondDeptDatabase.searchEmployeeByServiceLength(3, 6, 10));
    }
}