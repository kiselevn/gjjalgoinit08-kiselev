package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(pow(sc.nextInt(), sc.nextDouble()));
    }

    static double pow(int n, double a) {
        return n == 1 ? a : a * pow(n - 1, a);
    }
}