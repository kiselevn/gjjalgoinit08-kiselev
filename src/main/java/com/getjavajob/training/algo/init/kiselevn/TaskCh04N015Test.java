package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N015.getYear;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N15Test", 3, getYear(2007, 5, 2011, 2));
    }
}