package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getResultA(sc.nextInt()));
    }

    public static boolean getResultA(int input) {
        return input % 2 == 0;
    }

    public static boolean getResultB(int input) {
        return input % 2 != 0;
    }
}