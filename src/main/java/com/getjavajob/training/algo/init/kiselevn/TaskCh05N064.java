package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of districts");
        int numOfDistricts = sc.nextInt();
        int[] database = new int[numOfDistricts*2];
        for (int i = 0; i < numOfDistricts*2; i += 2) {
            System.out.println("Enter population of the " + (i / 2 + 1) + " district");
            database[i] = sc.nextInt();
            System.out.println("Enter population density of the " + (i / 2 + 1) + " district");
            database[i + 1] = sc.nextInt();
        }
        avgDensity(database,numOfDistricts);
    }

    static double avgDensity(int[] database,int numOfDistricts) {
        double totalSurface = 0;
        double totalDensity = 0;
        for (int i = 0; i < numOfDistricts; i++) {
            totalSurface += database[i] / database[i + 1];
            totalDensity += database[i];
        }
        return totalDensity / totalSurface;
    }
}