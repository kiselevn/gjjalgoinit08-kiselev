package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N053.reverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        double[] temp = {1, 2, 3, 4, 5, 6, 60};
        double[] temp2 = {60, 6, 5, 4, 3, 2, 1};
        assertEquals("TaskCh10N053Test", temp2, reverseArray(temp, 0));
    }
}