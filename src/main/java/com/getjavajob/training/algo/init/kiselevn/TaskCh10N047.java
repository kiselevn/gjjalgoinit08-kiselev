package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(fibonacci(sc.nextInt()));
    }

    static int fibonacci(int n) {
        return n == 0 || n == 1 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
    }
}