package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh10N050.ackermann;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N050Test", 7, ackermann(2, 2));
    }
}
