package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh05N064.avgDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh05N064Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[] database = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        assertEquals("TaskCh05N064", 1, avgDensity(database,12));
    }
}