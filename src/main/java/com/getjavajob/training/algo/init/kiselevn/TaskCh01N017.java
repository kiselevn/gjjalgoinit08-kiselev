package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

import static java.lang.Math.*;

public class TaskCh01N017 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getResultO(sc.nextDouble()));
    }

    static double getResultO(double x) {
        return sqrt(1 - pow(Math.sin(x), 2));
    }

    static double getResultP(double x, double a, double b, double c) {
        return 1 / sqrt(a * pow(x, 2) + b * x + c);
    }

    static double getResultR(double x) {
        return (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x));
    }

    static double getResultS(double x) {
        return abs(x) + abs(x + 1);
    }
}