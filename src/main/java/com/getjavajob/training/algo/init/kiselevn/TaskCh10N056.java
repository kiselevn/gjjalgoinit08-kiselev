package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh10N056 {
    public static void main(String args[]) {
        System.out.println(isPrime(90, 1));
    }

    static int isPrime(int input, int divisor) {
        if (input % divisor != 0 && divisor < input / 2) {
            input *= isPrime(input, divisor + 1);
        }
        if (input % (divisor + 1) == 0) {
            return 0;
        }
        return 1;
    }
}