package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(getCharAtK(sc.nextLine(), sc.nextInt()));
    }

    static char getCharAtK(String input, int k) {
        return input.charAt(k - 1);
    }
}