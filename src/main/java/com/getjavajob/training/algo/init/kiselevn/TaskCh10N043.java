package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(numberOfDigits(sc.nextInt()));
        System.out.println(digitSum(sc.nextInt()));
    }

    public static int digitSum(int n) {
        return n == 0 ? 0 : n % 10 + digitSum(n / 10);
    }

    public static int numberOfDigits(int n) {
        return n / 10 == 0 ? 1 : 1 + numberOfDigits(n / 10);
    }
}