package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh09N015.getCharAtK;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N015Test", 'k', getCharAtK("kach", 1));
    }
}