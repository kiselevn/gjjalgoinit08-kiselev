package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N024 {
    public static void main(String args[]) {
        int n = 5;
        int[][] input = getResultB(n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(input[i][j] + " ");
            }
            System.out.println();
        }
    }

    static int[][] getResultA(int n) {
        int[][] result = new int[n][n];
        for (int i = 0; i < n; i++) {
            result[0][i] = 1;
            result[i][0] = 1;
        }
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < n; j++) {
                result[i][j] = result[i - 1][j] + result[i][j - 1];
            }
        }
        return result;
    }

    static int[][] getResultB(int n) {
        int[][] result = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result[i][j] = (i + j) % n + 1;
            }
        }
        return result;
    }
}