package com.getjavajob.training.algo.init.kiselevn;

public class TaskCh12N063 {
    public static void main(String args[]) {
        int[][] input = new int[11][4];
        double[] result = getAverage(input);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    static double[] getAverage(int[][] input) {
        double[] result = new double[11];
        for (int i = 0; i < input.length; i++) {
            double sum = 0;
            for (int j = 0; j < input[1].length; j++) {
                sum += input[i][j];
            }
            result[i] = sum / input[1].length;
        }
        return result;
    }
}