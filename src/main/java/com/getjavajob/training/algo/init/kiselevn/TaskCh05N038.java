package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh05N038 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(strangeHusband(sc.nextInt()));
    }

    static double[] strangeHusband(int input) {
        double totalDistance = 0;
        for (double i = 1; i <= input; i++) {
            totalDistance += 1 / i;
        }
        double toHomeDistance = totalDistance;
        for (double i = 2; i <= input; i += 2) {
            toHomeDistance -= 2 / i;
        }
        double[] result = {totalDistance, toHomeDistance};
        return result;
    }
}