package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh02N043.divisiblityTest;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh02N043Test", 1, divisiblityTest(9, 3));
        assertEquals("TaskCh02N043Test", 2, divisiblityTest(10, 9));
    }
}