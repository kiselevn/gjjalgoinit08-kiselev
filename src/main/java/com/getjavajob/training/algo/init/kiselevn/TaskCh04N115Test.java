package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N115.chineseCalendar;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N115Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N115Test", "This year is the Red Monkey year", chineseCalendar(2016));
    }
}