package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N028.getResult;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N028Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[][] input = new int[5][5];
        int[][] expected = new int[][]{
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("TaskCh12N028 Test", expected, getResult(input, 5, 5));
    }
}