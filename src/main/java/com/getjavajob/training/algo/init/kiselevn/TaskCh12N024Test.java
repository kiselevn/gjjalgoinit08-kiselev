package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N024.getResultA;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh12N024.getResultB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        int[][] expected = new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("TaskCh12N024 test A", expected, getResultA(6));
        expected = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024 test B", expected, getResultB(6));
    }
}