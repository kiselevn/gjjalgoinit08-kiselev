package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh01N003.printNum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh01N003Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("Ch01N003Test", 7, printNum(7));
    }
}