package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(transferTable(sc.nextInt(),sc.nextInt()));
    }

    static double[] transferTable(int input,int tableLength) {
        double[] result = new double[tableLength];
        for (int i = 0; i < tableLength; i++) {
            result[i] = (i + 1) * input;
        }
        return result;
    }
}