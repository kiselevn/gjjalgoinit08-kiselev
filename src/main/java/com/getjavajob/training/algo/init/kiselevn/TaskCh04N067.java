package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(whatDayIsThis(sc.nextInt()));
    }

    static String whatDayIsThis(int input) {
        return (input - 1) % 7 < 5 ? "WorkingDay" : "Day off";
    }
}