package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        System.out.println(numReverse(input.nextShort()));
    }

    static int numReverse(int input) {
        return input / 100 + input % 10 * 100 + input / 10 % 10 * 10;
    }
}