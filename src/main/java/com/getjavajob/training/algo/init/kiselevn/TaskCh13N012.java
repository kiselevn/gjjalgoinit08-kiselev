package com.getjavajob.training.algo.init.kiselevn;

import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012 {
    public static void main(String args[]) {
        Employee firstOne = new Employee("Sonniy", "Nicko", "Ivanovich", "Moscow", 3, 10);
        Database secondDeptDatabase = new Database(firstOne);
        System.out.println(secondDeptDatabase.searchEmployeeByName("nicko").get(0).getName());
    }
}

class Employee {
    private String firstName;
    private String secondName;
    private String middleName;
    private String city;
    private int enrollmentYear;
    private int enrollmentMonth;

    public Employee(String secondName, String firstName, String city, int enrollmentYear, int enrollmentMonth) {
        this(secondName, firstName, "", city, enrollmentYear, enrollmentMonth);
    }

    public Employee(String secondName, String firstName, String middleName, String city, int enrollmentYear, int enrollmentMonth) {
        setMiddleName(middleName);
        setFirstName(firstName);
        setSecondName(secondName);
        setCity(city);
        setEnrollmentYear(enrollmentYear);
        setEnrollmentMonth(enrollmentMonth);
    }

    public String getName() {
        return secondName + " " + firstName + " " + middleName + " " + city;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getEnrollmentMonth() {
        return enrollmentMonth;
    }

    public void setEnrollmentMonth(int enrollmentMonth) {
        this.enrollmentMonth = enrollmentMonth;
    }

    public int getEnrollmentYear() {
        return enrollmentYear;
    }

    public void setEnrollmentYear(int enrollmentYear) {
        this.enrollmentYear = enrollmentYear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
}

class Database {
    private List<Employee> employees = new ArrayList<Employee>();

    public Database(List<Employee> input) {
        employees = input;
    }

    public Database(Employee input) {
        employees.add(input);
    }

    public void addEmployee(Employee input) {
        employees.add(input);
    }

    public List<Employee> searchEmployeeByServiceLength(int n, int currentYear, int currentMonth) {
        List<Employee> result = new ArrayList<Employee>();
        for (Employee instanceOfEmployee : employees) {
            if (instanceOfEmployee.getEnrollmentMonth() - currentMonth >= 0) {
                currentYear++;
            }
            if (currentYear - instanceOfEmployee.getEnrollmentYear() > n) {
                result.add(instanceOfEmployee);
            }
        }
        return result;
    }

    public List<Employee> searchEmployeeByName(String input) {
        List<Employee> result = new ArrayList<Employee>();
        for (Employee instanceOfEmployee : employees) {
            String fullName = (instanceOfEmployee.getFirstName() + instanceOfEmployee.getMiddleName() + instanceOfEmployee.getSecondName()).toLowerCase();
            if (fullName.contains(input.toLowerCase())) {
                result.add(instanceOfEmployee);
            }
        }
        return result;
    }
}