package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(isFirstAndLastSame(sc.nextLine()));
    }

    static boolean isFirstAndLastSame(String input) {
        return input.charAt(0) == input.charAt(input.length() - 1);
    }
}