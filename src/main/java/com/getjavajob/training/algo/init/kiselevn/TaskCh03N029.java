package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh03N029 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int X = input.nextInt();
        int Y = input.nextInt();
        int Z = input.nextInt();
        boolean result = getResultA(X, Y);
        System.out.println(result);
        result = getResultB(X, Y);
        System.out.println(result);
        result = getResultC(X, Y);
        System.out.println(result);
        result = getResultD(X, Y, Z);
        System.out.println(result);
        result = getResultE(X, Y, Z);
        System.out.println(result);
        result = getResultF(X, Y, Z);
        System.out.println(result);
    }

    /**@return return 1 if X and Y are odd*/
    static boolean getResultA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**@return return 1 if one among X and Y > 20*/
    static boolean getResultB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**@return return 1 if at least 1 among X and Y == 0*/
    static boolean getResultC(int x, int y) {
        return x == 0 || y == 0;
    }

    /**@return return 1 if X,Y,Z is negative*/
    static boolean getResultD(int x, int y, int z) {//
        return x < 0 && y < 0 && z < 0;
    }

    /**@return return 1 if only one among X,Y,Z divides on 5*/
    static boolean getResultE(int x, int y, int z) {
        boolean xDividesBy5 = x % 5 == 0;
        boolean yDividesBy5 = y % 5 == 0;
        boolean zDividesBy5 = z % 5 == 0;
        return !(xDividesBy5 && yDividesBy5 || xDividesBy5 && zDividesBy5 || yDividesBy5 && zDividesBy5) && (xDividesBy5 || yDividesBy5 || zDividesBy5);
    }

    /**@return return 1 if at least one among X,Y,Z >100*/
    static boolean getResultF(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}