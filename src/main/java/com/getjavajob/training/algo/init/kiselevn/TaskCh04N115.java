package com.getjavajob.training.algo.init.kiselevn;

import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println(chineseCalendar(sc.nextInt()));
    }

    static String chineseCalendar(int input) {
        String result = "This year is the";
        switch ((input + 56) % 10 / 2) {
            case 0:
                result += " Green";
                break;
            case 1:
                result += " Red";
                break;
            case 2:
                result += " Yellow";
                break;
            case 3:
                result += " White";
                break;
            case 4:
                result += " Black";
                break;
        }
        switch ((input + 56) % 12) {
            case 0:
                result += " Rat year";
                break;
            case 1:
                result += " Ox year";
                break;
            case 2:
                result += " Tiger year";
                break;
            case 3:
                result += " Rabbit year";
                break;
            case 4:
                result += " Dragon year";
                break;
            case 5:
                result += " Snake year";
                break;
            case 6:
                result += " Horse year";
                break;
            case 7:
                result += " Goat year";
                break;
            case 8:
                result += " Monkey year";
                break;
            case 9:
                result += " Rooster year";
                break;
            case 10:
                result += " Dog year";
                break;
            case 11:
                result += " Pig year";
                break;
        }
        return result;
    }
}