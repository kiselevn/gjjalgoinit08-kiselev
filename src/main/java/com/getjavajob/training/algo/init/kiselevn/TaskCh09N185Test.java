package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh09N185.bracesDoneRight;
import static com.getjavajob.training.algo.init.kiselevn.TaskCh09N185.wrongBraceNum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N185Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N185TestA", true, bracesDoneRight("()()"));
        assertEquals("TaskCh09N185TestA", 3, wrongBraceNum("()()((("));
    }
}