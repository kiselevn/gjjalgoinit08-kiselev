package com.getjavajob.training.algo.init.kiselevn;

import static com.getjavajob.training.algo.init.kiselevn.TaskCh04N036.trafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N036Test {
    public static void main(String args[]) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N036Test", "Red", trafficLight(3));
        assertEquals("TaskCh04N036Test", "Green", trafficLight(5));
    }
}