package com.getjavajob.training.algo.util;

import java.util.List;

public class Assert {
    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        boolean result = true;
        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != actual[i]) {
                result = false;
            }
        }
        if (result) {
            System.out.println(testName + " passed. Arrays are equal");
        } else {
            System.out.println(testName + " failed. Arrays are different");
        }
    }

    public static void assertEquals(String testName, String[] expected, String[] actual) {
        boolean result = true;
        for (int i = 0; i < expected.length; i++) {
            if (!(expected[i].equals(actual[i]))) {
                result = false;
            }
        }
        if (result) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed");
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        boolean result = true;
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                if (expected[i][j] != actual[i][j]) {
                    System.out.println(expected[i][j] + "!=" + actual[i][j]);
                    result = false;
                }
            }
        }
        if (result) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed");
        }
    }

    public static void assertEquals(String testName, List expected, List actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed");
        }
    }
}
